from django.urls import path
from . import views

urlpatterns = [
    path('free/', views.free_trucks, name='free_trucks'),
    path('assign/<int:truck_id>/', views.assign_truck, name='assign_truck'),
    path('assigned/', views.assigned_trucks, name='assigned_trucks'),
    path('complete/<int:truck_id>/', views.complete_task, name='complete'),
]
