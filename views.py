
from django.shortcuts import render, redirect
from .models import Truck


def free_trucks(request):
    trucks = Truck.objects.filter(is_assigned=False)
    return render(request, 'free_trucks.html', {'trucks': trucks})

def assign_truck(request, truck_id):
    truck = Truck.objects.get(id=truck_id)
    truck.is_assigned = True
    truck.assigned_to = 'Deliver Goods'  # Replace with the actual task name
    truck.save()
    return redirect('free_trucks')

def assigned_trucks(request):
    trucks = Truck.objects.filter(is_assigned=True)
    return render(request, 'assigned_trucks.html', {'trucks': trucks})

def complete_task(request, truck_id):
    truck = Truck.objects.get(id=truck_id)
    truck.is_assigned = False
    truck.assigned_to = ''
    truck.save()
    return redirect('assigned_trucks')


