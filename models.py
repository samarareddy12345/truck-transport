from django.db import models

class Truck(models.Model):
    number_plate = models.CharField(max_length=100)
    is_assigned = models.BooleanField(default=False)
    assigned_to = models.CharField(max_length=100, blank=True)